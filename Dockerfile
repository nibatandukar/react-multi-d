FROM node as build

WORKDIR /app

COPY . /app

RUN npm install

RUN npm run-script build

###########################################

FROM nginx:alpine

WORKDIR /usr/share/nginx/html/

COPY --from=build /app/build . 

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]

####################################################

